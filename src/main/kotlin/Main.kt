import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import java.io.PrintWriter

fun main() {
    PrintWriter("build/index.html").use {
        it.appendHTML().html {
            head {
                title("Client-server and Kotlin")
            }
            body {
                h1 {
                    +"Проект"
                }
                p {
                    +"Конспекты с поддержкой языка разметки Markdown"
                }
            }
        }
    }
}
